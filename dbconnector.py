from pymongo import MongoClient

class DbConnector:
    def __init__(self):
        self.client = MongoClient()
        self.feed = self.client.nfl.feed
        self.feed.create_index('id', unique=True)

    def upload_feed_entry(self, k):
        del k['published_parsed']
        del k['updated_parsed']
        if not self.feed.find_one({'id': k['id']}):
            self.feed.insert(k)

    def getNewsByTeam(self, team):
        return [i['title'] for i in self.feed.find({'source': team})]

if __name__ == '__main__':
    pass


