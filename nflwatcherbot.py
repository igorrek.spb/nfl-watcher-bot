from flask import Flask
import os
from dbconnector import DbConnector

app = Flask(__name__)
db = DbConnector()

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route("/")
def hello():
    return "Hello World!"

@app.route("/<team>/")
def getTeamNews(team):
    team = team.upper()
    header = team + ' News'
    return header + '<br>' + '<br>'.join(db.getNewsByTeam(team))

if __name__ == "__main__":
    app.run()
