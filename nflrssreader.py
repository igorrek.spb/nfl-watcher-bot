import feedparser, time
from multiprocessing import Process, ProcessError
from dbconnector import DbConnector
from random import randint


RSS_HOME = 'http://www.nfl.com/rss/rsslanding?searchString=home'
RSS_TEAMS = {
    "ARZ": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=ARZ",
    "ATL": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=ATL",
    "BAL": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=BAL",
    "BUF": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=BUF",
    "CAR": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=CAR",
    "CHI": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=CHI",
    "CIN": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=CIN",
    "CLV": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=CLV",
    "DAL": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=DAL",
    "DEN": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=DEN",
    "DET": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=DET",
    "GB": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=GB",
    "HOU": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=HOU",
    "IND": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=IND",
    "JAX": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=JAX",
    "KC": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=KC",
    "LA": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=LA",
    "MIA": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=MIA",
    "MIN": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=MIN",
    "NE": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=NE",
    "NO": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=NO",
    "NYG": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=NYG",
    "NYJ": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=NYJ",
    "OAK": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=OAK",
    "PHI": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=PHI",
    "PIT": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=PIT",
    "SD": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=SD",
    "SF": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=SF",
    "SEA": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=SEA",
    "TB": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=TB",
    "TEN": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=TEN",
    "WAS": "http://www.nfl.com/rss/rsslanding?searchString=team&abbr=WAS",
    }

class FeedReader():
    def __init__(self):
        self.db = None
        time.sleep(5)
        p = Process(target=self.parseInLoop)
        p.start()
        return

    def parseInLoop(self, timeout=1800):
        FEED_SLEEP = timeout

        if not self.db:
            self.db = DbConnector()
        while True:
            self.parseRss('HOME', RSS_HOME)
            for t in RSS_TEAMS:
                self.parseRss(t, RSS_TEAMS[t])
            time.sleep(FEED_SLEEP)


    def parseRss(self, key, rss):
        d = feedparser.parse(rss)
        for entry in d.get('entries', []):
            entry['source'] = key
            self.db.upload_feed_entry(entry)
        #print 'Parse rss for', key

if __name__ == '__main__':
    f = FeedReader()

